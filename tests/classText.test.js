const ClassText = require('../helper/text/text');
const eanglistWords = require('../const/eanglistWords');

test('test text class', () => {
  const Text = new ClassText(eanglistWords);

  expect(Text.getSortString('how how how lol lol lol ol lol.')).toBe('lol lol lol lol how how how ol');

  expect(Text.getStringWithoutSigns('how.&*')).toBe('how');

  expect(Text.hasQuestion('how are you')).toBeTruthy();
});
