const mongoose = require('mongoose');
const MessageSchema = require('../models/message');
const UsersSchema = require('../models/user');
const eanglistWords = require('../const/eanglistWords');
const config = require('../config/config');

test('models test', () => {
  mongoose.connect(config.mongoURI, { useNewUrlParser: true }, (err) => {
    if (err) {
      throw err;
    }
  });

  const user = new UsersSchema({
    name: 'Ivan',
    lastName: 'Haikhel',
  });

  user.save();

  const message = new MessageSchema({
    message: 'send ivan',
    user: user.id,
  });

  message.save();

  expect(message.hasQuestions(eanglistWords)).toBeTruthy();
  expect(user.hasQuestions()).toBeTruthy();
});
