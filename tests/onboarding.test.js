const OnBoarding = require('../controller/onBoarding/onBoarding');

test('test board class', () => {
  const newOnBoard = new OnBoarding('title', 'subTitle', [{ title: "123123", subTitle: 'asd123', icon: '123w' }])

  expect(newOnBoard.toJSON()).toEqual({
    title: 'title',
    subTitle: 'subTitle',
    answers: [{ title: '123123', subTitle: 'asd123', icon: 'asd123' }]
  })
});
