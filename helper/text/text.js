class Text {
  allQuestions;

  constructor(allQuestions) {
    this.allQuestions = allQuestions;
  }

  hasQuestion(text) {
    const allWord = text.split(' ');

    for (let i = 0; i < allWord.length; i++) {
      if (allWord[i].includes('?')) {
        return true;
      }

      if (this.allQuestions.find(
        (elem) => elem.toLowerCase() === this.getStringWithoutSigns(allWord[i].toLowerCase()),
      )) {
        return true;
      }
    }

    return false;
  }

  getStringWithoutSigns(text) {
    return text.replace(/[\s.!-_=+/*,%]/g, '');
  }

  getSortString(text) {
    const allWord = text.split(' ');

    const allCountWords = {};
    for (const el of allWord) {
      const word = this.getStringWithoutSigns(el.toLowerCase());
      if (!allCountWords[word]) {
        allCountWords[word] = {
          word: `${word} `,
          count: 0,
        };
      }
      allCountWords[word].count += 1;
    }

    const allCountWordsArray = Object.values(allCountWords).sort((a, b) => b.count - a.count);

    const result = allCountWordsArray.reduce((res, elem) => res + elem.word.repeat(elem.count), '');

    return result.trim();
  }
}

module.exports = Text;
