const mongoose = require('mongoose');
const config = require('../config/config');

mongoose.connect(config.mongoURI, { useNewUrlParser: true }, (err) => {
  if (err) {
    console.log(err);
  }
});
