const OnBoardingAnswer = require('./onBoardingAnswer');

class OnBoarding {
  title;

  subTitle;

  answers = [];

  constructor(title, subTitle, answers) {
    this.title = title;
    this.subTitle = subTitle;
    for (const elem of answers) {
      this.answers.push(new OnBoardingAnswer(elem))
    }

  }

  toJSON() {
    return {
      title: this.title,
      subTitle: this.subTitle,
      answers: this.answers.map((elem) => elem.get()),
    };
  }
}

module.exports = OnBoarding;
