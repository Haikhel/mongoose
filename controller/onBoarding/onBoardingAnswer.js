class OnBoardingAnswer {
  title;

  subTitle;

  icon;

  constructor(options) {
    this.title = options.title;
    this.subTitle = options.subTitle;
    this.icon = options.subTitle;
  }

  get() {
    return {
      title: this.title,
      subTitle: this.subTitle,
      icon: this.icon,
    };
  }
}

module.exports = OnBoardingAnswer;
