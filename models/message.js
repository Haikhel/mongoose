const mongoose = require('mongoose');

const { Schema } = mongoose;
const Text = require('../helper/text/text');

const messagesSchema = new Schema({
  message: {
    type: String,
    trim: true,
  },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
});

messagesSchema.methods.hasQuestions = async function hasQuestions(word) {
  const modelText = new Text(word);

  return modelText.hasQuestion(this.message);
};

module.exports = mongoose.model('messages', messagesSchema);
