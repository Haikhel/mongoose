const mongoose = require('mongoose');

const { Schema } = mongoose;
const messagesSchema = require('./message');

const usersSchema = new Schema({
  name: {
    type: String,
    trim: true,
  },
  lastName: {
    type: String,
    trim: true,
  },
  login: {
    type: String,
    trim: true,
  },
  password: {
    type: String,
    trim: true,
  },
});

usersSchema.methods.hasQuestions = async function hasQuestions() {
  const result = await messagesSchema.findOne().where({
    user: this.id,
  });

  if (result) {
    return true;
  }

  return false;
};

module.exports = mongoose.model('users', usersSchema);
