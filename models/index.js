const MessageSchema = require('./message');
const OnboardingSchema = require('./onboarding');
const OnboardingAnswerSchema = require('./onboardingAnswer');
const UserSchema = require('./user');

module.exports = {
  MessageSchema,
  OnboardingSchema,
  OnboardingAnswerSchema,
  UserSchema,
};
